/*
    -- HEFFTE (version 0.2) --
       Univ. of Tennessee, Knoxville
       @date
*/

#include "heffte.h"

int main(int argc, char *argv[]){

    /*
     * Tests if the single header can be included and compiled without
     * any other includes. Guards against bugs due to headers included
     * in the test common header but missing from the main heffte.h.
     */

    return 0;
}
