/*
    -- heFFTe (version 0.2) --
       Univ. of Tennessee, Knoxville
       @date
       Performance test for 3D FFTs using heFFTe
*/

#define BENCH_INPUT std::complex<precision_type>
#define BENCH_C2C

#include "speed3d.h"
